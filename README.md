# Note-takers v2.0

Our original project was to implement a notetaking app. Halfway through development, we changed it to a bitmap image processing application. 

## Description

Loads an image from Android's built-in Gallery app. The app allows the user to manipulate the image by applying a bitmap filter layer on the image.
This is done by triggering the algorithm through button clicks. Each algorithm manipulates the pixels on the bitmap based on the changed RGBA channels, where the colors are ranged between 0-255 HEX values. When the process is done the user then has the ability to change the image title and save edited to a JPEG or PNG File format. Once the save is complete, a pop-up notification will let them know it is successful stored in the Gallery. 

Filters:
- Color Inversion
- Saturation
- Horizontal Flip
- Vertical Flip
- Sepia
- Blue Tint
- Contrast 
- Greyscale
- Smoothing

Features:
- Login strings stored locally 
- Load image
- Select desired filter
- Filters can be combined for different effects
- Update image title for save
- Choose image file format: PNG/JPEG
- Save edited image back to Gallery
- Notification pop-up after successful save
- UX Notifications to prevent the user from selecting filter buttons and save button without loading an image

## Installation


Recommended requirements for Android Studio's emulation:
- Processor: AMD or Intel x86/x64
- RAM: 8GB RAM or higher
- HARD DISK: 2GB or higher
- GPU: 1280x800 screen resolution with integrated graphics or higher
- OS: Windows 10

We developed,tested, and ran this application using mostly Winodws 10 machines. 

In order to run this project Android Studio Arctic Fox 2020.3.1 is needed with an Android SDK version 11.0 R and gradle 11. The Android Virtual Device we used to run and test the app is the Pixel 2 API 30. 

To run this app simply open the directory that contains the application folder with all the android files(will not run if Anrdoid Studio cannot find the android dependencies, XML Files, Java Files within the main project folder), select the Pixel 2 AVD for the emulator and click "Run" to let the emulator load and launch the app.

Limitations are mostly due to the hardware and operating system that Android Studio is running on. The emulator can be resource instensive since it is running a virtual machine while the main operating system is running in the background. 

## Instructions:
1. Login with Username and Password alphanumeric characters greater than four.
2. Click "Confirm"
3. Click "Load Image"
4. Select stored image in the gallery app
5. Select one or multiple filters
6. Type name of image
7. Select image file format
8. Click "Save To Gallery" 

## Authors and credits
- Nicolas Lopez 
- Hector Ortiz
- Richard Rivas
- Sophia Galindo
- Ian Moskowitz
- Keith Choi

1. Nicolas: developed the Inverse, Horizontal/Vertical Flip, Blue Tint, and Contrast algorithms.  Was also the gitlab maintainer.   
2. Hector:  worked on the save/load bitmap, update image title and save to JPEG/PNG image file formats while working on error-handling. 
3. Richard: worked on the save notification, load/preview image from gallery features while also working on error-handling and assisting Hector and Sophia. 
4. Sophia:  worked on the front-end by changing the color schemes, button positioning, and general layout of the entire application. 
5. Ian:     worked on the Inversion and Sepia algorithm, login page, and researched bitmap functionalities. 
6. Keith:   created the Saturation, Grayscale, and Smoothing algorithms while also assisting Nicolas and Ian on researching the algorithms and how to manipulate pixels on the bitmap 

## Project status
As of the project status, it will completed by 12/11/21. The application should run with no crashes or bugs via the android studios application.
